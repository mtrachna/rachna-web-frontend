import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-page-privacy-policy',
  templateUrl: './page-privacy-policy.component.html',
  styleUrls: ['./page-privacy-policy.component.scss']
})
export class PagePrivacyPolicyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
