import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-page-create-ad',
  templateUrl: './page-create-ad.component.html',
  styleUrls: ['./page-create-ad.component.scss']
})
export class PageCreateAdComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
