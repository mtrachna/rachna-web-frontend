import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCreateAdComponent } from './page-create-ad.component';

describe('PageCreateAdComponent', () => {
  let component: PageCreateAdComponent;
  let fixture: ComponentFixture<PageCreateAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCreateAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCreateAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
