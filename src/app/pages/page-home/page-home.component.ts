import {Component, OnInit} from '@angular/core';
import {Startup} from '../../models/startup/startup';
import {StartupService} from '../../models/startup/startup.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'mt-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
