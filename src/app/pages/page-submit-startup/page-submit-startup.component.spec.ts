import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSubmitStartupComponent } from './page-submit-startup.component';

describe('PageSubmitStartupComponent', () => {
  let component: PageSubmitStartupComponent;
  let fixture: ComponentFixture<PageSubmitStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSubmitStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSubmitStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
