import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-page-submit-startup',
  templateUrl: './page-submit-startup.component.html',
  styleUrls: ['./page-submit-startup.component.scss']
})
export class PageSubmitStartupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
