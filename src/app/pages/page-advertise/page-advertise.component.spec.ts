import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAdvertiseComponent } from './page-advertise.component';

describe('PageAdvertiseComponent', () => {
  let component: PageAdvertiseComponent;
  let fixture: ComponentFixture<PageAdvertiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAdvertiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAdvertiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
