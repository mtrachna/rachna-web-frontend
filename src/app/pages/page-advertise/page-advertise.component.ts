import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-page-advertise',
  templateUrl: './page-advertise.component.html',
  styleUrls: ['./page-advertise.component.scss']
})
export class PageAdvertiseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
