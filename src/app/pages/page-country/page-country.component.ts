import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'mt-page-country',
  templateUrl: './page-country.component.html',
  styleUrls: ['./page-country.component.scss']
})

export class PageCountryComponent implements OnInit {

  private sub: any;
  public country: any;

  constructor(private route: ActivatedRoute) {
    this.sub = this.route.params.subscribe(params => {
      this.country = params['code'];
    });
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
}
