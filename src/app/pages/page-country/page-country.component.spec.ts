import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCountryComponent } from './page-country.component';

describe('PageCountryComponent', () => {
  let component: PageCountryComponent;
  let fixture: ComponentFixture<PageCountryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
