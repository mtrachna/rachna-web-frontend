import {Component, OnInit} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';
import {Startup} from '../../models/startup/startup';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'mt-page-startup',
  templateUrl: './page-startup.component.html',
  styleUrls: ['./page-startup.component.scss']
})
export class PageStartupComponent implements OnInit {

  env: boolean = environment.production;
  public startup: Startup;
  public loading = false;
  public success = false;
  private sub: any;
  public id: string;

  constructor(private route: ActivatedRoute,
              private startupService: StartupService) {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getStartup(this.id);
      window.scrollTo(0, 0);
    });
  }

  ngOnInit() {
  }

  getStartup(id: string) {

    this.loading = true;
    this.success = false;

    this.startupService.getById(id)
      .subscribe(
        data => {
          this.startup = data.json();
          this.loading = false;
          this.success = true;
        },
        error => {
          this.loading = false;
          this.success = false;
        });
  }

  goToLink() {
    window.open(this.startup.websiteUrl, '_blank');
  }

}
