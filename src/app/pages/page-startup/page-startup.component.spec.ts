import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageStartupComponent } from './page-startup.component';

describe('PageStartupComponent', () => {
  let component: PageStartupComponent;
  let fixture: ComponentFixture<PageStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
