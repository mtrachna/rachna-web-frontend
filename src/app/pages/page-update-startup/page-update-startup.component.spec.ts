import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUpdateStartupComponent } from './page-update-startup.component';

describe('PageUpdateStartupComponent', () => {
  let component: PageUpdateStartupComponent;
  let fixture: ComponentFixture<PageUpdateStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageUpdateStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUpdateStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
