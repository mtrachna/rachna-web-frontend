import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'mt-page-update-startup',
  templateUrl: './page-update-startup.component.html',
  styleUrls: ['./page-update-startup.component.scss']
})
export class PageUpdateStartupComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
