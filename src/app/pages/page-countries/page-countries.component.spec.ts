import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCountriesComponent } from './page-countries.component';

describe('PageCountriesComponent', () => {
  let component: PageCountriesComponent;
  let fixture: ComponentFixture<PageCountriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCountriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCountriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
