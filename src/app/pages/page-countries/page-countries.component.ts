import {Component, OnInit} from '@angular/core';
import {CountriesService} from '../../services/countries.service';

@Component({
  selector: 'mt-page-countries',
  templateUrl: './page-countries.component.html',
  styleUrls: ['./page-countries.component.scss']
})
export class PageCountriesComponent implements OnInit {

  public countries = [];
  public countriesFiltered = [];

  constructor(private countriesService: CountriesService) {
    this.countries = countriesService.getCountries();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.search('');
  }

  search(term) {
    this.countriesFiltered = this.countries.filter(
      country => country.name.toLowerCase().match(term));
  }

}
