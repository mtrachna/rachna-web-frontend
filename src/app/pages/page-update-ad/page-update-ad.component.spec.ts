import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUpdateAdComponent } from './page-update-ad.component';

describe('PageUpdateAdComponent', () => {
  let component: PageUpdateAdComponent;
  let fixture: ComponentFixture<PageUpdateAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageUpdateAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUpdateAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
