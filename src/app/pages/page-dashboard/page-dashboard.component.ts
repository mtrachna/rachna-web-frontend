import {Component, OnInit, ViewChild} from '@angular/core';
import {TotalStartupsComponent} from '../../components/total-startups/total-startups.component';
import {TotalAdsComponent} from '../../components/total-ads/total-ads.component';
import {TotalSubscribersComponent} from '../../components/total-subscribers/total-subscribers.component';
import {TotalStartupSubmissionsComponent} from '../../components/total-startup-submissions/total-startup-submissions.component';

@Component({
  selector: 'mt-page-dashboard',
  templateUrl: './page-dashboard.component.html',
  styleUrls: ['./page-dashboard.component.scss']
})
export class PageDashboardComponent implements OnInit {

  @ViewChild(TotalStartupsComponent) totalStartupsComponent: TotalStartupsComponent;
  @ViewChild(TotalAdsComponent) totalAdsComponent: TotalAdsComponent;
  @ViewChild(TotalSubscribersComponent) totalSubscribersComponent: TotalSubscribersComponent;
  @ViewChild(TotalStartupSubmissionsComponent) totalStartupSubmissionsComponent: TotalStartupSubmissionsComponent;

  constructor() {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  actionAllStartups(e) {
    this.totalStartupsComponent.getTotalStartups();
  }

  actionAllAds(e) {
    this.totalAdsComponent.getTotalAds();
  }

  actionAllSubscribers(e) {
    this.totalSubscribersComponent.getTotalSubscribers();
  }

  actionAllSubmissions(e) {
    this.totalStartupSubmissionsComponent.getTotalSubmissions();
  }

}
