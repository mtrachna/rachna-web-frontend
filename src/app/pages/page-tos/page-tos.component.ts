import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-page-tos',
  templateUrl: './page-tos.component.html',
  styleUrls: ['./page-tos.component.scss']
})
export class PageTosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
