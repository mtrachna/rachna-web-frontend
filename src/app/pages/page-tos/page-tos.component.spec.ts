import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTosComponent } from './page-tos.component';

describe('PageTosComponent', () => {
  let component: PageTosComponent;
  let fixture: ComponentFixture<PageTosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
