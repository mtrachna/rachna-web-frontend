import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PageCreateStartupComponent} from './page-create-startup.component';

describe('PageFrmStartupComponent', () => {
  let component: PageCreateStartupComponent;
  let fixture: ComponentFixture<PageCreateStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageCreateStartupComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCreateStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
