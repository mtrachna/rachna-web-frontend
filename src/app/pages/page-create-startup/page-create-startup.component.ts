import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'mt-page-create-startup',
  templateUrl: './page-create-startup.component.html',
  styleUrls: ['./page-create-startup.component.scss']
})
export class PageCreateStartupComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
