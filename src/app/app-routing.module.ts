import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PageHomeComponent} from './pages/page-home/page-home.component';
import {PageStartupComponent} from './pages/page-startup/page-startup.component';
import {PageLoginComponent} from './pages/page-login/page-login.component';
import {PageCreateStartupComponent} from './pages/page-create-startup/page-create-startup.component';
import {AuthGuard} from './guards/auth.guard';
import {PageDashboardComponent} from './pages/page-dashboard/page-dashboard.component';
import {PageCreateAdComponent} from './pages/page-create-ad/page-create-ad.component';
import {PageTosComponent} from './pages/page-tos/page-tos.component';
import {PagePrivacyPolicyComponent} from './pages/page-privacy-policy/page-privacy-policy.component';
import {PageAdvertiseComponent} from './pages/page-advertise/page-advertise.component';
import {PageSubmitStartupComponent} from './pages/page-submit-startup/page-submit-startup.component';
import {PageUpdateStartupComponent} from './pages/page-update-startup/page-update-startup.component';
import {PageUpdateAdComponent} from './pages/page-update-ad/page-update-ad.component';
import {PageCountryComponent} from './pages/page-country/page-country.component';
import {PageCountriesComponent} from './pages/page-countries/page-countries.component';

const routes: Routes = [
  {path: '', component: PageHomeComponent},
  {path: 'startups/:id', component: PageStartupComponent},
  {path: 'login', component: PageLoginComponent},
  {path: 'startup/create', component: PageCreateStartupComponent, canActivate: [AuthGuard]},
  {path: 'startup/submit', component: PageSubmitStartupComponent},
  {path: 'startup/update/:id', component: PageUpdateStartupComponent, canActivate: [AuthGuard]},
  {path: 'ad/create', component: PageCreateAdComponent, canActivate: [AuthGuard]},
  {path: 'ad/update/:id', component: PageUpdateAdComponent, canActivate: [AuthGuard]},
  {path: 'dashboard', component: PageDashboardComponent, canActivate: [AuthGuard]},
  {path: 'country/:code', component: PageCountryComponent},
  {path: 'countries', component: PageCountriesComponent},
  {path: 'privacy-policy', component: PagePrivacyPolicyComponent},
  {path: 'advertise', component: PageAdvertiseComponent},
  {path: 'submit-startup', redirectTo: 'startup/submit'},
  {path: 'tos', component: PageTosComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
