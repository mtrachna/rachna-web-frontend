import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PageHomeComponent} from './pages/page-home/page-home.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FooterComponent} from './components/footer/footer.component';
import {TitleComponent} from './components/title/title.component';
import {BtnOneComponent} from './components/btn-one/btn-one.component';
import {StartupCardComponent} from './components/startup-card/startup-card.component';
import {BtnTwoComponent} from './components/btn-two/btn-two.component';
import {StartupService} from './models/startup/startup.service';
import {PageStartupComponent} from './pages/page-startup/page-startup.component';
import {TitleStartupComponent} from './components/title-startup/title-startup.component';
import {ImgStartupComponent} from './components/img-startup/img-startup.component';
import {DescStartupComponent} from './components/desc-startup/desc-startup.component';
import {PageLoginComponent} from './pages/page-login/page-login.component';
import {FrmLoginComponent} from './components/frm-login/frm-login.component';
import {FormsModule} from '@angular/forms';
import {FrmStartupComponent} from './components/frm-startup/frm-startup.component';
import {PageCreateStartupComponent} from './pages/page-create-startup/page-create-startup.component';
import {AuthenticationService} from './services/authentication.service';
import {HttpModule} from '@angular/http';
import {AuthGuard} from './guards/auth.guard';
import {SpinnerOneComponent} from './components/spinner-one/spinner-one.component';
import {SearchBoxComponent} from './components/search-box/search-box.component';
import {SearchService} from './services/search.service';
import {EditorComponent} from './components/editor/editor.component';
import {ShareComponent} from './components/share/share.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {SidebarService} from './services/sidebar.service';
import {FrmNewsletterComponent} from './components/frm-newsletter/frm-newsletter.component';
import {NewsletterService} from './models/newsletter/newsletter.service';
import {DisqusModule} from 'ngx-disqus';
import {SpinnerTwoComponent} from './components/spinner-two/spinner-two.component';
import {PageDashboardComponent} from './pages/page-dashboard/page-dashboard.component';
import {Sidebar2Component} from './components/sidebar-2/sidebar-2.component';
import {Navbar2Component} from './components/navbar-2/navbar-2.component';
import {TotalStartupsComponent} from './components/total-startups/total-startups.component';
import {CardComponent} from './components/card/card.component';
import {TotalSubscribersComponent} from './components/total-subscribers/total-subscribers.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material';
import {TableComponent} from './components/table/table.component';
import {RecommendationsComponent} from './components/recommendations/recommendations.component';
import {MathService} from './services/math.service';
import {AdComponent} from './components/ad/ad.component';
import {FrmAdComponent} from './components/frm-ad/frm-ad.component';
import {PageCreateAdComponent} from './pages/page-create-ad/page-create-ad.component';
import {AdService} from './models/ad/ad.service';
import {TotalAdsComponent} from './components/total-ads/total-ads.component';
import {PagePrivacyPolicyComponent} from './pages/page-privacy-policy/page-privacy-policy.component';
import {PageTosComponent} from './pages/page-tos/page-tos.component';
import {PageAdvertiseComponent} from './pages/page-advertise/page-advertise.component';
import {PageSubmitStartupComponent} from './pages/page-submit-startup/page-submit-startup.component';
import {TotalStartupSubmissionsComponent} from './components/total-startup-submissions/total-startup-submissions.component';
import {AllStartupsComponent} from './components/all-startups/all-startups.component';
import {AllSubscribersComponent} from './components/all-subscribers/all-subscribers.component';
import {AllAdsComponent} from './components/all-ads/all-ads.component';
import {HttpService} from './services/http.service';
import {AllStartupsCardsComponent} from './components/all-startups-cards/all-startups-cards.component';
import {SocialComponent} from './components/social/social.component';
import {PageUpdateStartupComponent} from './pages/page-update-startup/page-update-startup.component';
import {PageUpdateAdComponent} from './pages/page-update-ad/page-update-ad.component';
import {CountriesService} from './services/countries.service';
import {PageCountryComponent} from './pages/page-country/page-country.component';
import {TitleCountryComponent} from './components/title-country/title-country.component';
import {PageCountriesComponent} from './pages/page-countries/page-countries.component';
import {ColorService} from './services/color.service';

@NgModule({
  declarations: [
    AppComponent,
    PageHomeComponent,
    NavbarComponent,
    FooterComponent,
    TitleComponent,
    BtnOneComponent,
    StartupCardComponent,
    BtnTwoComponent,
    PageStartupComponent,
    TitleStartupComponent,
    ImgStartupComponent,
    DescStartupComponent,
    PageLoginComponent,
    FrmLoginComponent,
    FrmStartupComponent,
    PageCreateStartupComponent,
    SpinnerOneComponent,
    SearchBoxComponent,
    EditorComponent,
    ShareComponent,
    SidebarComponent,
    FrmNewsletterComponent,
    SpinnerTwoComponent,
    PageDashboardComponent,
    Sidebar2Component,
    Navbar2Component,
    TotalStartupsComponent,
    CardComponent,
    TotalSubscribersComponent,
    TableComponent,
    RecommendationsComponent,
    AdComponent,
    FrmAdComponent,
    PageCreateAdComponent,
    TotalAdsComponent,
    PagePrivacyPolicyComponent,
    PageTosComponent,
    PageAdvertiseComponent,
    PageSubmitStartupComponent,
    TotalStartupSubmissionsComponent,
    AllStartupsComponent,
    AllSubscribersComponent,
    AllAdsComponent,
    AllStartupsCardsComponent,
    SocialComponent,
    PageUpdateStartupComponent,
    PageUpdateAdComponent,
    PageCountryComponent,
    TitleCountryComponent,
    PageCountriesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    BrowserAnimationsModule,
    MatTableModule,
    DisqusModule.forRoot('rachnaio')
  ],
  providers: [
    StartupService,
    SearchService,
    AuthenticationService,
    SidebarService,
    NewsletterService,
    AdService,
    MathService,
    HttpService,
    CountriesService,
    ColorService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
