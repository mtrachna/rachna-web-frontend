import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {AuthenticationService} from '../services/authentication.service';
import {Http, Response} from '@angular/http';
import {Injector} from '@angular/core';

@Injectable()
export class HttpService {

  public endpoint = '';
  private route = '';

  protected http: Http;
  protected authenticationService: AuthenticationService;

  constructor(injector: Injector) {
    this.http = injector.get(Http);
    this.authenticationService = injector.get(AuthenticationService);
  }

  setEndpoint(endpoint: string) {
    this.endpoint = endpoint;
    this.route = `${environment.apiHost}/api/${this.endpoint}`;
  }

  get(options?: any) {

    const url = `${this.route}${this.jsonToQueryString(options)}`;

    return this.http.get(url)
      .map((response: Response) => response);
  }


  private jsonToQueryString(json) {
    if (!json) {
      return '';
    }
    return '?' +
      Object.keys(json).map(function (key) {
        return key + '=' + json[key];
      }).join('&');
  }

  getCount(options?: any) {

    const url = `${this.route}/count${this.jsonToQueryString(options)}`;

    return this.http.get(url)
      .map((response: Response) => response);
  }

  getById(id) {
    return this.http.get(`${this.route}/${id}`)
      .map((response: Response) => response);
  }

  update(id, data) {
    return this.http.put(`${this.route}/${id}`, data)
      .map((response: Response) => response);
  }

  create(item) {
    return this.http.post(`${this.route}`, item).map((response: Response) => response).share();
  }

  delete(id) {
    return this.http.delete(`${this.route}/${id}`)
      .map((response: Response) => response);
  }

}
