import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class SidebarService {

  private iconClicked = new Subject<boolean>();
  iconClicked$ = this.iconClicked.asObservable();

  private iconClicked2 = new Subject<boolean>();
  iconClicked2$ = this.iconClicked2.asObservable();

  constructor() {
  }

  toggleSidebar(iconClicked: boolean) {
    this.iconClicked.next(iconClicked);
  }

  toggleSidebar2(iconClicked: boolean) {
    this.iconClicked2.next(iconClicked);
  }
}
