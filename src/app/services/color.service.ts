import {Injectable} from '@angular/core';
import {MathService} from './math.service';

@Injectable()
export class ColorService {

  private colors = ['#C0C0C0', '#808080', 'tomato', '#800000', '#808000', '#008000', '#008080',
    '#000080', '#800080', '#C39BD3', '#3498DB', '#1ABC9C', '#52BE80', '#F1C40F', '#F39C12', '#D35400', '#BDC3C7', '#7F8C8D',
    '#566573', '#2C3E50'];

  constructor(private mathService: MathService) {
  }

  getRandomColor() {
    return this.colors[this.mathService.getRandomInt(0, this.colors.length)];
  }
}
