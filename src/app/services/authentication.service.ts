import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthenticationService {
  constructor(private http: Http) {
  }

  login(email, password) {

    return this.http.post(environment.apiHost + '/api/Users/login', {
      email: email,
      password: password
    })
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        const user = response.json();

        if (user && user.id) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
      })
      .flatMap((userInfo) => {
        return this.http.get(`${environment.apiHost}/api/Users/${this.getUserId()}?access_token=${this.getAccessToken()}`, {});
      }).map((response: Response) => {

        const userInfo = response.json();

        if (userInfo && userInfo.id) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('userInfo', JSON.stringify(userInfo));
        }
      });
  }

  private loginUser(email, password) {

    const url = `${environment.apiHost}/api/Users/login`;
    return this.http.post(url, {
      email: email,
      password: password
    }).map((response: Response) => response);
  }

  private getUserInfo() {

  }

  logout() {
    const params: URLSearchParams = new URLSearchParams();
    params.set('access_token', this.getAccessToken());

    return this.http.post(environment.apiHost + '/api/Users/logout?access_token=' + this.getAccessToken(), {})
      .map((response: Response) => response);
  }

  getAccessToken() {
    if (localStorage.getItem('currentUser')) {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      return currentUser.id.toString() || '';
    }
  }

  getUserId() {
    if (localStorage.getItem('currentUser')) {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      return currentUser.userId.toString() || '';
    }
  }

  isAuthenticated() {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
  }

  /**
   * private helper methods
   * @returns {RequestOptions}
   */
  getJwtRequestOptions() {
    // create authorization header with jwt token
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.id) {
      const headers = new Headers({'Authorization': 'Bearer ' + currentUser.id});
      return new RequestOptions({headers: headers});
    }
  }
}
