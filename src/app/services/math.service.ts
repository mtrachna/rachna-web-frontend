import {Injectable} from '@angular/core';

@Injectable()
export class MathService {

  constructor() {
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

}
