import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {SidebarService} from '../../services/sidebar.service';

@Component({
  selector: 'mt-navbar-2',
  templateUrl: './navbar-2.component.html',
  styleUrls: ['./navbar-2.component.scss']
})
export class Navbar2Component implements OnInit {

  constructor(private authenticationService: AuthenticationService,
              private sidebarService: SidebarService,
              private router: Router) {
  }

  ngOnInit() {

  }

  logout() {
    // logout
    this.authenticationService.logout().subscribe(
      data => {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userInfo');
        this.router.navigate(['']);
      },
      error => {
      });
  }

  toggleSidebar(e) {
    this.sidebarService.toggleSidebar2(true);
  }

}
