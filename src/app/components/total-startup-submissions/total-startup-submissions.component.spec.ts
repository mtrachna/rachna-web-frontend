import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalStartupSubmissionsComponent } from './total-startup-submissions.component';

describe('TotalStartupSubmissionsComponent', () => {
  let component: TotalStartupSubmissionsComponent;
  let fixture: ComponentFixture<TotalStartupSubmissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalStartupSubmissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalStartupSubmissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
