import {Component, OnInit} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';

@Component({
  selector: 'mt-total-startup-submissions',
  templateUrl: './total-startup-submissions.component.html',
  styleUrls: ['./total-startup-submissions.component.scss']
})
export class TotalStartupSubmissionsComponent implements OnInit {

  count = 0;
  loading = false;
  success = false;

  constructor(private startupService: StartupService) {
  }

  getTotalSubmissions() {
    this.loading = true;
    this.success = false;

    const options = {
      'where[status]': 'SUBMITTED'
    };

    this.startupService.getCount(options).subscribe(data => {
      this.count = data.json().count;
      this.loading = false;
      this.success = true;
    });
  }

  ngOnInit() {
    this.getTotalSubmissions();
  }

}
