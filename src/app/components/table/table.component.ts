import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

@Component({
  selector: 'mt-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() config: any;
  @Output() action: EventEmitter<object> = new EventEmitter<object>();

  model: any = {};

  count = 0;
  loading = false;
  success = false;

  limit = 8;
  offset = 0;

  data: Array<any> = [];

  searchFormDisplay = 'none';

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.getData();
  }

  private getData(id?: string) {
    this.loading = true;
    this.success = false;

    const options = {
      'filter[limit]': this.limit,
      'filter[skip]': this.offset,
      'filter[order]': 'modified DESC'
    };

    if (this.model.query) {
      const pattern = new RegExp('^' + this.model.query, 'i');
      const searchKey = `filter[where][${id}][regexp]`;
      options[searchKey] = pattern || '';
    }

    const data = this.config.service.get(options).map(res => res.json());
    const count = this.config.service.getCount(options).map(res => res.json());

    Observable.forkJoin([count, data]).subscribe((result: any[]) => {

      this.count = result[0].count;
      this.data = result[1];
      this.loading = false;
      this.success = true;
    });
  }

  delete(e, i) {
    this.config.service.delete(this.data[i].id).subscribe(
      data => {
        this.action.emit({
          type: 'delete'
        });
        this.getData();
      },
      error => {
      });
  }

  edit(e, i) {
    this.router.navigate([`/${this.config.editRoute}/update`, this.data[i].id]);
  }

  next(e) {
    if (!(this.offset + this.limit >= this.count)) {
      this.offset += this.limit;
    }

    this.getData();
  }

  previous(e) {
    if (this.offset - this.limit <= 0) {
      this.offset = 0;
    } else {
      this.offset -= this.limit;
    }
    this.getData();
  }

  search(id) {
    this.limit = 8;
    this.offset = 0;
    this.getData(id);

  }

  toggleSearchBox() {
    if (this.searchFormDisplay === 'none') {
      this.searchFormDisplay = 'flex';
    } else {
      this.searchFormDisplay = 'none';
    }
  }

  closeSearch() {
    this.searchFormDisplay = 'none';
  }

}
