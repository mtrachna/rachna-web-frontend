import {Component, OnInit} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';

@Component({
  selector: 'mt-total-startups',
  templateUrl: './total-startups.component.html',
  styleUrls: ['./total-startups.component.scss']
})
export class TotalStartupsComponent implements OnInit {

  count = 0;
  loading = false;
  success = false;

  constructor(private startupService: StartupService) {
  }

  getTotalStartups() {
    this.loading = true;
    this.success = false;

    this.startupService.getCount().subscribe(data => {
      this.count = data.json().count;
      this.loading = false;
      this.success = true;
    });
  }

  ngOnInit() {
    this.getTotalStartups();
  }

}
