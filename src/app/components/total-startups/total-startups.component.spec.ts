import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalStartupsComponent } from './total-startups.component';

describe('TotalStartupsComponent', () => {
  let component: TotalStartupsComponent;
  let fixture: ComponentFixture<TotalStartupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalStartupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalStartupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
