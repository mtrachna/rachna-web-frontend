import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'mt-frm-login',
  templateUrl: './frm-login.component.html',
  styleUrls: ['./frm-login.component.scss']
})
export class FrmLoginComponent implements OnInit {

  model: any = {};
  loading = false;
  success = false;
  clientValidationDone = false;

  constructor(private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.success = false;
    this.authenticationService.login(this.model.email, this.model.password)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
          this.clientValidationDone = true;
          this.router.navigate(['/dashboard']);
        },
        error => {
          this.loading = false;
          this.success = false;
          this.clientValidationDone = true;
        });
  }

}
