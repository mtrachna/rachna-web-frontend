import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';
import {Startup} from '../../models/startup/startup';

@Component({
  selector: 'mt-all-startups',
  templateUrl: './all-startups.component.html',
  styleUrls: ['./all-startups.component.scss']
})
export class AllStartupsComponent implements OnInit {

  @Output() action: EventEmitter<string> = new EventEmitter<string>();

  public config = {
    title: {
      enabled: true,
      title: 'All Startups'
    },
    header: {
      enabled: true
    },
    pagination: {
      enabled: true
    },
    columns: [
      {title: 'Startup', id: 'name', format: 'prop'},
      {title: 'URL', id: 'websiteUrl', format: 'prop'},
      {title: 'Status', id: 'status', format: 'prop'},
      {title: 'Edit', format: 'edit'},
      {title: 'Delete', format: 'delete'}
    ],
    service: this.service,
    search: {
      enabled: true,
      searchProp: 'name'
    },
    editRoute: 'startup'
  };

  constructor(private service: StartupService) {
  }

  ngOnInit() {
  }

  handleTableActions(e) {
    if (e.type === 'delete') {
      this.delete(e.index);
    }
  }

  private delete(i: number) {
    this.action.emit('delete');
  }

}
