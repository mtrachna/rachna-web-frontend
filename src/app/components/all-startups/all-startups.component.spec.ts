import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllStartupsComponent } from './all-startups.component';

describe('AllStartupsComponent', () => {
  let component: AllStartupsComponent;
  let fixture: ComponentFixture<AllStartupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllStartupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllStartupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
