import {Component, Input, OnInit} from '@angular/core';
import {Startup} from '../../models/startup/startup';
import {Router, RouterLink} from '@angular/router';

@Component({
  selector: 'mt-startup-card',
  templateUrl: './startup-card.component.html',
  styleUrls: ['./startup-card.component.scss']
})
export class StartupCardComponent implements OnInit {

  @Input()
  startup: Startup;

  imgLoading = true;

  onLoad() {
    this.imgLoading = false;
  }

  constructor(private  router: Router) {
  }

  ngOnInit() {
  }

  goToCountry() {
    this.router.navigate(['/country', this.startup.country]);
  }
}
