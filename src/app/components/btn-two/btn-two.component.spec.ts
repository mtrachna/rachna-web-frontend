import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnTwoComponent } from './btn-two.component';

describe('BtnTwoComponent', () => {
  let component: BtnTwoComponent;
  let fixture: ComponentFixture<BtnTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
