import {Component, Input, OnInit} from '@angular/core';
import {ColorService} from '../../services/color.service';

@Component({
  selector: 'mt-btn-two',
  templateUrl: './btn-two.component.html',
  styleUrls: ['./btn-two.component.scss']
})
export class BtnTwoComponent implements OnInit {

  bgColor: string;
  @Input() bgRandom;

  constructor(private colorService: ColorService) {

  }

  ngOnInit() {
    if (this.bgRandom) {
      this.bgColor = this.colorService.getRandomColor();
    }
  }

}
