import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalAdsComponent } from './total-ads.component';

describe('TotalAdsComponent', () => {
  let component: TotalAdsComponent;
  let fixture: ComponentFixture<TotalAdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalAdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
