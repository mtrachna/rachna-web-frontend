import {Component, OnInit} from '@angular/core';
import {AdService} from '../../models/ad/ad.service';

@Component({
  selector: 'mt-total-ads',
  templateUrl: './total-ads.component.html',
  styleUrls: ['./total-ads.component.scss']
})
export class TotalAdsComponent implements OnInit {

  count = 0;
  loading = false;
  success = false;

  constructor(private adService: AdService) {
  }

  getTotalAds() {
    this.loading = true;
    this.success = false;

    this.adService.getCount().subscribe(data => {
      this.count = data.json().count;
      this.loading = false;
      this.success = true;
    });
  }

  ngOnInit() {
    this.getTotalAds();
  }

}
