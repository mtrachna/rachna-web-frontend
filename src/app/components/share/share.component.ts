import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mt-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit {

  @Input()
  url = 'https://rachna.io/';

  public shareUrl: string;

  constructor() {
  }

  ngOnInit() {
    this.encodeUrl();
  }

  encodeUrl() {
    const eurl = encodeURIComponent(this.url);
    this.shareUrl = eurl; // `https://www.facebook.com/sharer/sharer.php?u=${eurl}&amp;src=sdkpreparse`;
  }

}
