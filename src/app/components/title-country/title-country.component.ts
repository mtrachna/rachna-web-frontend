import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CountriesService} from '../../services/countries.service';

@Component({
  selector: 'mt-title-country',
  templateUrl: './title-country.component.html',
  styleUrls: ['./title-country.component.scss']
})
export class TitleCountryComponent implements OnInit {

  private sub: any;
  private id: string;
  public country: any;

  constructor(private route: ActivatedRoute,
              private countriesService: CountriesService) {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['code'];
      this.country = countriesService.getCountryByCode(this.id).name;
    });
  }

  ngOnInit() {
  }

}
