import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleCountryComponent } from './title-country.component';

describe('TitleCountryComponent', () => {
  let component: TitleCountryComponent;
  let fixture: ComponentFixture<TitleCountryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TitleCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
