import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mt-spinner-one',
  templateUrl: './spinner-one.component.html',
  styleUrls: ['./spinner-one.component.scss']
})
export class SpinnerOneComponent implements OnInit {

  @Input() lbl: string;

  constructor() {
  }

  ngOnInit() {
  }

}
