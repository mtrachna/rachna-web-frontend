import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerOneComponent } from './spinner-one.component';

describe('SpinnerOneComponent', () => {
  let component: SpinnerOneComponent;
  let fixture: ComponentFixture<SpinnerOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpinnerOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
