import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescStartupComponent } from './desc-startup.component';

describe('DescStartupComponent', () => {
  let component: DescStartupComponent;
  let fixture: ComponentFixture<DescStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
