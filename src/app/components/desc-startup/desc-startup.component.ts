import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mt-desc-startup',
  templateUrl: './desc-startup.component.html',
  styleUrls: ['./desc-startup.component.scss']
})
export class DescStartupComponent implements OnInit {

  @Input()
  desc: string;

  constructor() {
  }

  ngOnInit() {
  }

}
