import {Component, OnInit} from '@angular/core';
import {SidebarService} from '../../services/sidebar.service';

@Component({
  selector: 'mt-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private sidebarService: SidebarService) {
  }

  ngOnInit() {
  }

  goToLink() {
    window.open('mailto:letstalk@rachna.io', '_blank');
  }

  toggleSidebar(e) {
    this.sidebarService.toggleSidebar(true);
  }

}
