import {Component, OnInit} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';
import {NewsletterService} from '../../models/newsletter/newsletter.service';

@Component({
  selector: 'mt-total-subscribers',
  templateUrl: './total-subscribers.component.html',
  styleUrls: ['./total-subscribers.component.scss']
})
export class TotalSubscribersComponent implements OnInit {

  count = 0;
  loading = false;
  success = false;

  constructor(private newsletterService: NewsletterService) {
  }

  getTotalSubscribers() {
    this.loading = true;
    this.success = false;

    this.newsletterService.getCount().subscribe(data => {
      this.count = data.json().count;
      this.loading = false;
      this.success = true;
    });
  }

  ngOnInit() {
    this.getTotalSubscribers();
  }

}
