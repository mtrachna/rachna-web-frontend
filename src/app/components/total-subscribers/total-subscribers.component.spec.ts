import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalSubscribersComponent } from './total-subscribers.component';

describe('TotalSubscribersComponent', () => {
  let component: TotalSubscribersComponent;
  let fixture: ComponentFixture<TotalSubscribersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalSubscribersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
