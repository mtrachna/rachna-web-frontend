import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FrmAdComponent} from './frm-ad.component';

describe('FrmAdComponent', () => {
  let component: FrmAdComponent;
  let fixture: ComponentFixture<FrmAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrmAdComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrmAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
