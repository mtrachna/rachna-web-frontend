import {Component, Input, OnInit} from '@angular/core';
import {AdService} from '../../models/ad/ad.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'mt-frm-ad',
  templateUrl: './frm-ad.component.html',
  styleUrls: ['./frm-ad.component.scss']
})
export class FrmAdComponent implements OnInit {

  title = 'Create';
  model: any = {};
  loading = false;
  success = false;
  clientValidationDone = false;
  private sub: any;
  private id: string;
  @Input() mode: string;

  public statuses = [
    {value: 'ARCHIVED', display: 'Archived'},
    {value: 'SUBMITTED', display: 'Submitted'},
    {value: 'PUBLISHED', display: 'Published'}
  ];

  constructor(private route: ActivatedRoute,
              private service: AdService) {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit() {
    if (this.mode === 'update') {
      this.title = 'Update';
      this.get(this.id);
    } else if (this.mode === 'create') {
      this.title = 'Create';
    }
  }

  submit(f) {
    if (this.mode === 'update') {
      this.update(f, this.id);
    } else if (this.mode === 'create' || this.mode === 'submit') {
      this.create(f);
    }
  }

  get(id: string) {
    this.service.getById(id)
      .subscribe(
        data => {
          this.model = data.json();
        },
        error => {
        });
  }

  create(f) {
    this.loading = true;
    this.success = false;
    f.form.markAsUntouched();

    this.service.create(this.model)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
          this.clientValidationDone = true;
          f.resetForm();
          f.form.reset();
        },
        error => {
          this.loading = false;
          this.success = false;
          this.clientValidationDone = true;
        },
        () => {
        });
  }

  update(f, id) {

    this.loading = true;
    this.success = false;

    this.service.update(id, this.model)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
          this.clientValidationDone = true;
        },
        error => {
          this.loading = false;
          this.success = false;
          this.clientValidationDone = true;
        },
        () => {
        });
  }

}
