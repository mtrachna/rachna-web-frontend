import {Component, OnInit} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';
import {Startup} from '../../models/startup/startup';
import {MathService} from '../../services/math.service';

@Component({
  selector: 'mt-recommendations',
  templateUrl: './recommendations.component.html',
  styleUrls: ['./recommendations.component.scss']
})
export class RecommendationsComponent implements OnInit {

  startups: Startup[];
  loading = false;
  success = false;

  limit = 4;
  offset = 0;

  constructor(private startupService: StartupService,
              private mathService: MathService) {
  }

  getStartups() {
    this.loading = true;
    this.success = false;

    const options = {
      'filter[limit]': this.limit,
      'filter[skip]': this.offset,
      'where[status]': 'PUBLISHED',
      'filter[order]': 'modified DESC'
    };

    this.startupService.get(options).map(res => res.json()).subscribe(data => {
      this.startups = data;
      this.loading = false;
      this.success = true;
      this.offset += this.limit;
    });
  }

  ngOnInit() {
    this.offset = this.mathService.getRandomInt(1, 25);
    this.getStartups();
  }

}
