import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mt-spinner-two',
  templateUrl: './spinner-two.component.html',
  styleUrls: ['./spinner-two.component.scss']
})
export class SpinnerTwoComponent implements OnInit {

  @Input() lbl: string;

  constructor() {
  }

  ngOnInit() {
  }

}
