import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerTwoComponent } from './spinner-two.component';

describe('SpinnerTwoComponent', () => {
  let component: SpinnerTwoComponent;
  let fixture: ComponentFixture<SpinnerTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpinnerTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
