import {Component, OnInit} from '@angular/core';
import {SidebarService} from '../../services/sidebar.service';

@Component({
  selector: 'mt-sidebar-2',
  templateUrl: './sidebar-2.component.html',
  styleUrls: ['./sidebar-2.component.scss']
})
export class Sidebar2Component implements OnInit {

  constructor(private sidebarService: SidebarService) {
    this.sidebarService.iconClicked2$.subscribe(
      data => {

        const ele1 = <HTMLElement> document.querySelector('.sidebar-2');
        const eleNav2 = <HTMLElement> document.querySelector('.navbar-2');
        const eleNav3 = <HTMLElement> document.querySelector('.main-content');

        if (ele1.style.left === '0px') {
          ele1.style.left = '-250px';
          eleNav2.style.left = '0px';
          eleNav2.style.right = '0px';
          eleNav3.style.right = '0px';
          eleNav3.style.left = '0px';
        } else {
          ele1.style.left = '0px';
          eleNav2.style.left = '250px';
          eleNav2.style.right = '-250px';
          eleNav3.style.right = '-250px';
          eleNav3.style.left = '250px';
        }

        /*const ele2 = <HTMLElement> document.querySelector('.main-content');
        ele2.style.marginLeft = '250px';*/
      }
    );
  }

  ngOnInit() {
  }

}
