import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgStartupComponent } from './img-startup.component';

describe('ImgStartupComponent', () => {
  let component: ImgStartupComponent;
  let fixture: ComponentFixture<ImgStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
