import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mt-img-startup',
  templateUrl: './img-startup.component.html',
  styleUrls: ['./img-startup.component.scss']
})
export class ImgStartupComponent implements OnInit {

  @Input()
  imgSrc: string;

  imgLoading = true;

  onLoad() {
    this.imgLoading = false;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
