import {Component, OnInit} from '@angular/core';
import {Ad} from '../../models/ad/ad';
import {AdService} from '../../models/ad/ad.service';

@Component({
  selector: 'mt-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {

  ads: Ad[];
  loading = false;
  success = false;

  constructor(private adService: AdService) {
  }

  getAds() {
    this.loading = true;
    this.success = false;

    const options = {
      'filter[where][status]': 'PUBLISHED'
    };

    this.adService.get(options).map(res => res.json()).subscribe(data => {
      this.ads = data;
      this.loading = false;
      this.success = true;
    });
  }

  ngOnInit() {
    this.getAds();
  }

}
