import {Component, Input, OnInit} from '@angular/core';
import {StartupService} from '../../models/startup/startup.service';
import {ActivatedRoute} from '@angular/router';
import {CountriesService} from '../../services/countries.service';

@Component({
  selector: 'mt-frm-startup',
  templateUrl: './frm-startup.component.html',
  styleUrls: ['./frm-startup.component.scss']
})

export class FrmStartupComponent implements OnInit {

  title = 'Create';
  model: any = {};
  loading = false;
  success = false;
  clientValidationDone = false;
  private sub: any;
  private id: string;
  @Input() mode: string;
  countries: any;

  public plans = [
    {value: 'FREE', display: 'Free'},
    {value: 'PRO', display: 'Pro'},
    {value: 'BUSINESS', display: 'Business'}
  ];

  public statuses = [
    {value: 'ARCHIVED', display: 'Archived'},
    {value: 'PROMOTED', display: 'Promoted'},
    {value: 'SUBMITTED', display: 'Submitted'},
    {value: 'PUBLISHED', display: 'Published'}
  ];

  constructor(private route: ActivatedRoute,
              private service: StartupService,
              private countriesService: CountriesService) {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit() {

    this.countries = this.countriesService.getCountries();

    if (this.mode === 'update') {
      this.title = 'Update';
      this.get(this.id);
    } else if (this.mode === 'create') {
      this.title = 'Create';
    } else if (this.mode === 'submit') {
      this.title = 'Submit';
      this.model.status = 'SUBMITTED';
    }
  }

  submit(f) {
    if (this.mode === 'update') {
      this.update(f, this.id);
    } else if (this.mode === 'create' || this.mode === 'submit') {
      this.create(f);
    }
  }

  create(f) {

    this.loading = true;
    this.success = false;
    f.form.markAsUntouched();

    this.service.create(this.model)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
          this.clientValidationDone = true;
          f.resetForm();
          f.form.reset();
        },
        error => {
          this.loading = false;
          this.success = false;
          this.clientValidationDone = true;
        },
        () => {
        });
  }

  update(f, id) {

    this.loading = true;
    this.success = false;

    this.service.update(id, this.model)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
          this.clientValidationDone = true;
        },
        error => {
          this.loading = false;
          this.success = false;
          this.clientValidationDone = true;
        },
        () => {
        });
  }

  get(id: string) {

    this.service.getById(id)
      .subscribe(
        data => {
          this.model = data.json();
        },
        error => {
        });
  }

}
