import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FrmStartupComponent} from './frm-startup.component';

describe('FrmStartupComponent', () => {
  let component: FrmStartupComponent;
  let fixture: ComponentFixture<FrmStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrmStartupComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrmStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
