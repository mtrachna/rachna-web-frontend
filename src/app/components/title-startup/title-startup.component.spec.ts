import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleStartupComponent } from './title-startup.component';

describe('TitleStartupComponent', () => {
  let component: TitleStartupComponent;
  let fixture: ComponentFixture<TitleStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TitleStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
