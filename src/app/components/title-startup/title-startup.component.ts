import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mt-title-startup',
  templateUrl: './title-startup.component.html',
  styleUrls: ['./title-startup.component.scss']
})
export class TitleStartupComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  subTitle: string;

  constructor() {
  }

  ngOnInit() {
  }

}
