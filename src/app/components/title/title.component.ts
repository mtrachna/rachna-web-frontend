import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as Typed from 'typed.js';

@Component({
  selector: 'mt-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

    const options = {
      strings: ['India', 'USA', 'China', 'Singapore', 'Israel', 'Finland', 'Sri Lanka', 'Canada', 'Earth', 'Europe'],
      typeSpeed: 250,
      backDelay: 1000,
      loop: true,
      shuffle: true
    };

    const typed = new Typed('.typed', options);

  }

}
