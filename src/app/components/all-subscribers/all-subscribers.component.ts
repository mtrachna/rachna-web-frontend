import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NewsletterService} from '../../models/newsletter/newsletter.service';

@Component({
  selector: 'mt-all-subscribers',
  templateUrl: './all-subscribers.component.html',
  styleUrls: ['./all-subscribers.component.scss']
})
export class AllSubscribersComponent implements OnInit {

  @Output() action: EventEmitter<string> = new EventEmitter<string>();

  public config = {
    title: {
      enabled: true,
      title: 'All Newsletter Subscribers'
    },
    header: {
      enabled: true
    },
    pagination: {
      enabled: true
    },
    columns: [
      {title: 'Email', id: 'email', format: 'prop'},
      {title: 'Delete', format: 'delete'}
    ],
    service: this.service,
    search: {
      enabled: true,
      searchProp: 'email'
    }
  };

  constructor(private service: NewsletterService) {
  }

  ngOnInit() {
  }

  handleTableActions(e) {
    if (e.type === 'delete') {
      this.delete(e.index);
    }
  }

  private delete(i: number) {
    this.action.emit('delete');
  }

}
