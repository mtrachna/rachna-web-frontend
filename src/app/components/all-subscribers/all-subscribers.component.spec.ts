import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSubscribersComponent } from './all-subscribers.component';

describe('AllSubscribersComponent', () => {
  let component: AllSubscribersComponent;
  let fixture: ComponentFixture<AllSubscribersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllSubscribersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
