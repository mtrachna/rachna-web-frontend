import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'mt-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {

  private searchUpdated: Subject<string> = new Subject<string>();

  @Output() searchChangeEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    this.searchChangeEmitter = <any>this.searchUpdated.asObservable().debounceTime(200)
      .distinctUntilChanged();
  }

  ngOnInit() {
  }

  public onSearchType(value: string) {
    console.log(value);
    this.searchUpdated.next(value);
  }
}
