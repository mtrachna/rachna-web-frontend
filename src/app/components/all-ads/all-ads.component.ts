import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AdService} from '../../models/ad/ad.service';

@Component({
  selector: 'mt-all-ads',
  templateUrl: './all-ads.component.html',
  styleUrls: ['./all-ads.component.scss']
})
export class AllAdsComponent implements OnInit {

  @Output() action: EventEmitter<string> = new EventEmitter<string>();

  public config = {
    title: {
      enabled: true,
      title: 'All Ads'
    },
    header: {
      enabled: true
    },
    pagination: {
      enabled: true
    },
    columns: [
      {title: 'Description', id: 'desc', format: 'prop'},
      {title: 'Edit', format: 'edit'},
      {title: 'Delete', format: 'delete'}
    ],
    service: this.service,
    search: {
      enabled: true,
      searchProp: 'desc'
    },
    editRoute: 'ad'
  };

  constructor(private service: AdService) {
  }

  ngOnInit() {
  }

  handleTableActions(e) {
    if (e.type === 'delete') {
      this.delete(e.index);
    }
  }

  private delete(i: number) {
    this.action.emit('delete');
  }

}
