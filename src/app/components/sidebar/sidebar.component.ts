import {Component, HostBinding, OnInit} from '@angular/core';
import {SidebarService} from '../../services/sidebar.service';
import {style} from '@angular/animations';
import {Router} from '@angular/router';

@Component({
  selector: 'mt-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @HostBinding('style.display')
  display = 'none';

  constructor(private sidebarService: SidebarService,
              private router: Router) {
    this.sidebarService.iconClicked$.subscribe(
      data => {
        this.display = 'block';
        document.body.style.overflow = 'hidden';
        document.documentElement.style.overflow = 'hidden';
        (document.querySelector('mt-root')as HTMLElement).style.overflow = 'hidden';
      }
    );
  }

  closeSidebar() {
    this.display = 'none';
    document.body.style.overflow = 'auto';
    document.documentElement.style.overflow = 'auto';

    (document.querySelector('mt-root')as HTMLElement).style.overflow = 'auto';
  }

  ngOnInit() {
  }

  goToHome() {
    this.closeSidebar();
    this.router.navigate(['/']);
  }

  goToCountries() {
    this.closeSidebar();
    this.router.navigate(['/countries']);
  }

  goToSubmitStartup() {
    this.closeSidebar();
    this.router.navigate(['/startup/submit']);
  }

}
