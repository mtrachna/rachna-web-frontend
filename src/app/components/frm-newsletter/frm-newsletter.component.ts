import {Component, OnInit} from '@angular/core';
import {NewsletterService} from '../../models/newsletter/newsletter.service';

@Component({
  selector: 'mt-frm-newsletter',
  templateUrl: './frm-newsletter.component.html',
  styleUrls: ['./frm-newsletter.component.scss']
})
export class FrmNewsletterComponent implements OnInit {

  model: any = {};
  loading = false;
  success = false;
  clientValidationDone = false;

  constructor(private newsletterService: NewsletterService) {
  }

  ngOnInit() {
  }

  submit(f) {
    if (!!this.model.test) {
      return;
    }

    this.loading = true;
    this.success = false;

    this.newsletterService.create(this.model)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
          this.clientValidationDone = true;
          f.form.reset();
        },
        error => {
          this.loading = false;
          this.success = false;
          this.clientValidationDone = true;
        });
  }

}
