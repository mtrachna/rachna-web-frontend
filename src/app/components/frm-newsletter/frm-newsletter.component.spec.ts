import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrmNewsletterComponent } from './frm-newsletter.component';

describe('FrmNewsletterComponent', () => {
  let component: FrmNewsletterComponent;
  let fixture: ComponentFixture<FrmNewsletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrmNewsletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrmNewsletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
