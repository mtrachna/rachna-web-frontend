import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {StartupService} from '../../models/startup/startup.service';
import {Startup} from '../../models/startup/startup';

@Component({
  selector: 'mt-all-startups-cards',
  templateUrl: './all-startups-cards.component.html',
  styleUrls: ['./all-startups-cards.component.scss']
})
export class AllStartupsCardsComponent implements OnInit {

  @Input() status: string;
  @Input() country: string;
  @Input() today: boolean;

  todaysDate = new Date();

  spinnerLbl: string;

  startups: Startup[] = [];
  count = 0;
  loading = false;
  success = false;

  limit = 8;
  offset = 0;

  constructor(private startupService: StartupService) {

  }

  getStartups() {
    this.loading = true;
    this.success = false;

    const getOptions = {
      'filter[limit]': this.limit,
      'filter[skip]': this.offset,
      'filter[order]': 'modified DESC'
    };

    const countOptions = {};

    if (this.status) {
      getOptions['filter[where][status]'] = this.status;
      countOptions['[where][status]'] = this.status;
    }

    if (this.country) {
      getOptions['filter[where][country]'] = this.country;
      countOptions['[where][country]'] = this.country;
    }

    if (this.today) {
      const todayMidNight = new Date();
      todayMidNight.setHours(0, 0, 0, 0);
      const todayMidNightString = todayMidNight.toISOString();
      getOptions['filter[where][modified][gt]'] = todayMidNightString;
      countOptions['[where][modified][gt]'] = todayMidNightString;
    }

    if (this.status === 'PUBLISHED') {
      const todayMidNight = new Date();
      todayMidNight.setHours(0, 0, 0, 0);
      const yesterdayMidNightString = todayMidNight.toISOString();
      getOptions['filter[where][modified][lt]'] = yesterdayMidNightString;
      countOptions['[where][modified][lt]'] = yesterdayMidNightString;
    }

    const count = this.startupService.getCount(countOptions).map(res => res.json());
    const startups = this.startupService.get(getOptions).map(res => res.json());

    Observable.forkJoin([count, startups]).subscribe(data => {
      this.count = data[0].count;
      this.startups = this.startups.concat(data[1]);
      this.loading = false;
      this.success = true;
      this.offset += this.limit;
    });
  }

  ngOnInit() {
    if (this.status === 'PROMOTED') {
      this.limit = 4;
      this.spinnerLbl = 'trending startups';
    } else if (this.today) {
      this.spinnerLbl = 'today\'s startups';
    } else {
      this.spinnerLbl = 'awesome startups';
    }

    this.getStartups();
  }

}
