import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllStartupsCardsComponent } from './all-startups-cards.component';

describe('AllStartupsCardsComponent', () => {
  let component: AllStartupsCardsComponent;
  let fixture: ComponentFixture<AllStartupsCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllStartupsCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllStartupsCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
