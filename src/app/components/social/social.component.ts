import {Component, Input, OnInit} from '@angular/core';

declare var $: any;

@Component({
  selector: 'mt-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {

  @Input() fb: string;
  @Input() twitter: string;
  @Input() googlePlus: string;
  @Input() instagram: string;
  @Input() linkedIn: string;
  @Input() url = 'https://rachna.io/';

  public shareUrl: string;

  constructor() {
  }

  ngOnInit() {
    this.initPopoup();
    this.encodeUrl();
  }

  initPopoup() {
    $('[data-toggle="popover"]').popover({
      html: true,
      content: function () {
        return $('.popover-content').html();
      }
    });
  }

  encodeUrl() {
    const eurl = encodeURIComponent(this.url);
    this.shareUrl = eurl; // `https://www.facebook.com/sharer/sharer.php?u=${eurl}&amp;src=sdkpreparse`;
  }

}
