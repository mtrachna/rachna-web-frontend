import {Component, OnInit} from '@angular/core';
import {SidebarService} from './services/sidebar.service';
import {NavigationEnd, Router} from '@angular/router';
import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'mt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public currentRoute: string;

  constructor(private sidebarService: SidebarService,
              public authenticationService: AuthenticationService,
              public router: Router) {
  }

  ngOnInit() {
  }

  toggleSidebar(e) {
    this.sidebarService.toggleSidebar(false);
  }
}
