export class Ad {
  id: String;
  desc: string;
  status: string;
  created: Date;
  modified: Date;
}
