import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../services/http.service';

@Injectable()
export class NewsletterService extends HttpService {

  constructor(injector: Injector) {
    super(injector);
    this.setEndpoint('Newsletters');
  }
}
