export class Startup {
  id: String;
  name: string;
  desc: string;
  subTitle: string;
  websiteUrl: string;
  imgUrlSmall: string;
  imgUrlLarge: string;
  linkedInUrl: string;
  fbUrl: string;
  twitterUrl: string;
  googlePlusUrl: string;
  instagramUrl: string;
  status: string;
  country: string;
  plan: string;
  submitterEmail: string;
  created: Date;
  modified: Date;
}
